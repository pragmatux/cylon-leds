#!/usr/bin/make -f

prefix=/usr

PACKAGE=$(shell dh_listpackages)
ALL:=cylon-leds

all: $(ALL)

clean:
	rm -f $(ALL)

$(ALL): $(ALL).cpp

install: $(ALL)
	$(INSTALL) -D --target-directory=$(DESTDIR)$(prefix)/bin $(ALL)

.PHONY: clean all install

